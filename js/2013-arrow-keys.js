document.querySelector('#video-panel-container').dataset.offset = 0;
var next2013, prev2013, offsetReplacement2013;


if (!prev2013 && !next2013 && !offsetReplacement2013) {
    offsetReplacement2013 = offsetReplacement2013 || function (offsetMathHandlerF, outOfBoundsF) {
        const elem = document.querySelector('#video-panel-container');
        if (!elem) {
            return;
        }

        const offs = parseInt(elem.dataset.offset);
        if (outOfBoundsF(offs)) {
            return;
        }

        const newIndex = offsetMathHandlerF(offs);
        elem.classList.replace(`offs${offs}`, `offs${newIndex}`);
        elem.dataset.offset = newIndex;
    };

    next2013 = next2013 || function () {
        offsetReplacement2013(offset => offset + 1, offset => offset >= 2)
    };

    prev2013 = prev2013 || function () {
        offsetReplacement2013(offset => offset - 1, offset => offset <= 0)
    };

    $(document).keydown((event) => {
        if (event.key === "ArrowLeft") {
            prev2013();
        } else if (event.key === "ArrowRight") {
            next2013();
        }
    });
}

document.querySelectorAll('div.video div.panel .next').forEach(n => n.addEventListener('click', next2013));
document.querySelectorAll('div.video div.panel .prev').forEach(n => n.addEventListener('click', prev2013)); 