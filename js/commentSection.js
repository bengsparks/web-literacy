var avatarPathsAndNames;

if (!avatarPathsAndNames) {
    avatarPathsAndNames = [
        ["DollBomber", "imgs/avatar/small/bulletblink.gif"],
        ["CatLover70", "imgs/avatar/small/katze.gif"],
        ["ilovegardening", "imgs/avatar/small/rose.gif"],
        ["ProfessionalSnowballer", "imgs/avatar/small/schneeman.gif"],
    ]
}

function addComment(input) {
    const writtenText = typeof input !== "string"
        ? $("#input-field > textarea").val()
        : input;
    if (writtenText.length === 0) {
        return;
    }

    const [userName, avatarPath] = avatarPathsAndNames[Math.floor(Math.random() * avatarPathsAndNames.length)];

    const comment = $("<div>");
    comment.addClass("comment");

    const avatar = $("<a>");
    avatar.addClass("avatar");

    const img = $("<img>");
    img.attr("src", avatarPath);

    avatar.append(img);

    // TODO: Add default image

    const content = $("<div>");
    content.addClass("content");

    const author = $("<a>");
    author.addClass("author");
    author.text(userName);

    const text = $("<div>");
    text.addClass("text");

    text.text(writtenText);

    $("#input-field > textarea").val("");

    content.append(author);
    content.append(text);

    comment.append(avatar);
    comment.append(content);

    // console.log(comment);

    $("#reply-form").before(comment);
}

$("#submit-button").on("click", addComment);


$("document").ready(() => {
    $('.ui.dropdown')
        .dropdown();

    const commentTexts = ["Nach der Jahrtausendwende wurde das Internet besonders durch eine Entwicklung geprägt: das Web 2.0. Usern war es nun möglich, eigene Inhalte hochzuladen und mit anderen Menschen über das Internet in Kontakt zu treten.",
        "Dadurch entstanden Blogs und Wikis und vor allem wurden soziale Netzwerke geschaffen. Menschen mit denselben Interessen oder Ansichten konnten sich von da an untereinander austauschen und sich eigene Inhalte gegenseitig zugänglich machen. Durch diese Entwicklung wurden unter anderem neue Geschäftsmodelle ins Leben gerufen, wie beispielsweise Ebay. (vgl. Die Entwicklung des Internets, S. 97)",
        "Durch Web 2.0 entstanden zahlreiche neue Websites und Plattformen, wie beispielsweise Facebook und MySpace im Jahr 2004 (vgl. Shelley 2019). Im selben Jahr wurde zudem ein neuer Webbrowser entwickelt, Mozilla Firefox, dessen erste Version Webstandards besser unterstützen konnte und eine höhere Sicherheit bot.",
        "Dieser freie Webbrowser entwickelte sich mit der Zeit zu einem der meistbenutzten Browser weltweit. Im Bereich der Gestaltung von Websites ist rund um das Jahr 2003 ebenfalls ein neuer Meilenstein zu finden. Content Management Systeme wurden gelauncht, wie beispielsweise WordPress, welches mittlerweile eines der bekanntesten CMS der Welt ist. (vgl. Web Design History Timeline) Dadurch wurde es immer einfacher, Inhalte einer Website zu verändern. Hierfür waren nun keine Programmierfähigkeiten mehr nötig.",
        "Im Bereich des Webdesigns gab es viele Veränderungen. Ab 2002 war es möglich, mit Adobe Flash Video Content in Websites zu integrieren. Dadurch konnte der Inhalt einer Seite kreativ und anschaulich für den User gestaltet werden. (vgl. Adobe Flash)",
        "Menüleisten wurden nun mehrheitlich am oberen Rand einer Seite platziert und ab 2002 lagen Drop-Down-Menüs im Trend, wobei bei einem Mausklick oder einem Mouseover bei einem Menüpunkt ein Untermenü nach unten aufklappte. (vgl. Valeanu 2018)",
        "Ab 2003 wurden Drag-and-Drop-Builder beliebt. Dadurch konnte der User leicht ein Objekt mit der Maus anfassen und woanders hinschieben. (vgl. Die Entwicklung des Internets). Ebenso im Trend lagen nach wie vor Splash Pages sowie Flash Animation Intro Pages. Durch eine attraktiv gestaltete Startseite versuchte man, das Interesse der Nutzer zu wecken, sodass sie sich weiterhin auf einer Seite aufhielten. Anfang der 2000er Jahre lag der Fokus im Webdesign auf Funktionalität und Lesbarkeit.",
        "Das Ziel war es also, dem User die Nutzung einer Website zu erleichtern, weshalb Inhalte auf das Wesentliche reduziert wurden und man darauf achtete, Webseiten nicht mehr zu überladen, damit der Nutzer sich zurechtfinden konnte. Im Jahr 2003 waren 38 Millionen Websites weltweit online. (vgl. Kruse 2017)"
    ]

    commentTexts.forEach(addComment);
})
