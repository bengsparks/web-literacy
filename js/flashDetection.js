function isFlashEnabled() {
    var flash = navigator.plugins.namedItem('Shockwave Flash');
    if (!flash) { return 0; }
    else { return 1; }
}

$("#video-player").ready(function () {
    if (!isFlashEnabled()) {
        document.getElementById("video-player").replaceWith(
            "Flash is deprecated nowadays!"
        );
    }
})