$(document).ready(() => {
    const $topLeftRail = $("#top-left-rail");
    const $topRightRail = $("#top-right-rail");

    $topLeftRail.click(() => {
        $(".ui.secondary.menu > .item")[1].click()
    });

    $topRightRail.click(() => {
        $(".ui.secondary.menu > .item")[3].click()
    });
});