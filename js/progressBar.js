"use strict";

const progressBarConfig = {
    _errorIfSet: function (attr) {
        if (attr !== undefined) {
            throw `attribute @ ${attr} is already set!`
        }
    },

    setRange: function (total) {
        this.total = this._errorIfSet(this.total) || total;
        return this;
    },

    setLabel: function (label) {
        this.label = this._errorIfSet(this.label) || label;
        return this;
    },

    setValues: function (values) {
        this.values = this._errorIfSet(this.values) || values;
        return this;
    },

    setColors: function (colors) {
        if (this.values === undefined) {
            throw `Cannot set colors if setTotal was not executed!`;
        } else if (this.values.length !== this.colors.length) {
            throw `this.values.length @ ${this.values.length} != this.colors.length: ${this.colors.length}!`;
        }

        this.colors = this._errorIfSet(this.colors) || colors;
        return this;
    }
};


const progressBar = {
    initialize: function (insertionPoint, config) {
        this.values = config.values;
        this.dataIndex = 0;

        let container = $("<div>").addClass("ui progress indicating");

        let progressBar = $("<div>").addClass("bar")
            .append($("<div>").addClass("progress"));
        let label = $("<div>").addClass("label")
            .append(config.label);


        container.append(progressBar, label);
        $(insertionPoint).append(container);

        this._updateStyle(this.values[0], this.values[0]);
        this.goToIndex(this.dataIndex);
    },

    _errIfNotInBounds: function (index) {
        if (index < 0 || index >= this.values.length) {
            throw `willBeInBounds failed with index ${index}`;
        }
    },

    _updateStyle: function (oldValue, newValue) {
        $(".ui.progress")
            .removeClass(`year${oldValue}`)
            .addClass(`year${newValue}`)
        ;

        if (this.dataIndex === 0) {
            $(".ui.progress")
                .removeClass("success")
                .removeClass("active")
            ;
        }
    },

    goToValue: function (value) {
        if (typeof value !== "number") {
            value = parseInt(value, 10);
        }

        const idx = this.values.indexOf(value);
        const realIdx = idx === -1
            ? 8 : idx;

        this.goToIndex(realIdx);
    },

    goToIndex: function (index) {
        this._errIfNotInBounds(index);
        if (index === this.dataIndex) {
            return;
        }

        const oldValue = this.values[this.dataIndex];

        console.log(this.values);
        const pct = (this.values[index] - this.values[0]) /
            (2021 - this.values[0]) * 100;
            

        $(".ui.progress").progress("set percent", pct);
        this.dataIndex = index;

        const newValue = this.values[this.dataIndex];

        console.log(`percentage = ${pct}`);
        console.log(`index = ${this.dataIndex}`);

        this._updateStyle(oldValue, newValue);
    }
};

$(document).ready(function () {
    const data = $.map($(".ui.menu > .item"), x => parseInt(x.innerText, 10))
        .sort((lhs, rhs) => lhs - rhs)
        .filter(x => !isNaN(x));
    data.push(2021);

    const dataRange = data[data.length - 1] - data[0];
    const config = progressBarConfig
        .setLabel("Timeline")
        .setRange(dataRange)
        .setValues(data);

    progressBar.initialize("#progress-bar-insertion-point", config);
});