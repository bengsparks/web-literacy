$(document).ready(() => {
    const $prevPageButton = $("#prev-page-button");
    const nextPageButton = $("#next-page-button");

    $prevPageButton.click(() => {
        $(".ui.secondary.menu > .item")[3].click()
    });

    nextPageButton.click(() => {
        $(".ui.secondary.menu > .item")[5].click()
    });
});