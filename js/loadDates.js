"use strict";

function loadDateAtInsertionPoint(date, insertionPoint) {
    /*return new Promise((resolve, reject) => {
        const dateHref = urljoin("html", `${date}.html`);

        $(insertionPoint).load(dateHref).onload = () => {
            console.log(`successfully loaded html: ${dateHref}`);
            resolve();
        };
    });*/

    const dateHref = urljoin("pages", `${date}.html`);
    $.get(dateHref)
        .done(() => {
            $(insertionPoint).load(dateHref);
            $(insertionPoint).removeClass();
            $(insertionPoint).addClass(`year${date.substr(0, date.indexOf("-"))}`);
            console.log(`successfully loaded ${dateHref}`);

            console.log(progressBar.values[progressBar.dataIndex]);

        })
        .fail(() => {
            throw `attempted to load non-existent href: ${dateHref}`;
        });
}


function loadDateCss(date) {
    return new Promise((resolve) => {
        const link = document.createElement("link");
        link.rel = "stylesheet";
        link.href = urljoin("css", `${date}.css`);

        document.head.append(link);
        link.onload = () => {
            console.log(`successfully loaded css: ${link.href}`);
            resolve();
        };
    });
}