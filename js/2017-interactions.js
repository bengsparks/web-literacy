document.querySelector('#cube2017').dataset.offset = 0;

var next2017, prev2017, offsetReplacement2017, colours2017;

if (!colours2017) {
    colours2017 = ["#BB85FC", "#3E50B4", "#03dac6", "#FF3F80"];
}


setTimeout(() => {
    const newElem = document.querySelector(`#cube2017 .face.s0 div`);
    const cw = newElem.offsetWidth;
    const ch = newElem.offsetHeight;

    document.querySelector('.overlay-sizer').style.width = `${cw}px`;
    document.querySelector('.overlay-sizer').style.height = `${ch}px`;
    document.querySelector('.overlay-sizer').style['border-color'] = colours2017[0];
});

if (!prev2017 && !next2017 && !offsetReplacement2017) {
    offsetReplacement2017 = offsetReplacement2017 || function (offsetMathHandlerF, outOfBoundsF) {
        const elem = document.querySelector('#cube2017');
        if (!elem) {
            return;
        }

        const offs = parseInt(elem.dataset.offset);
        if (outOfBoundsF(offs)) {
            return;
        }

        const newIndex = offsetMathHandlerF(offs);
        elem.classList.replace(`step${offs}`, `step${newIndex}`);
        elem.dataset.offset = newIndex;
        setTimeout(() => {
            const newElem = elem.querySelector(`.face.s${newIndex} div`);
            const cw = newElem.offsetWidth;
            const ch = newElem.offsetHeight;

            document.querySelector('.overlay-sizer').style.width = `${cw}px`;
            document.querySelector('.overlay-sizer').style.height = `${ch}px`;
            document.querySelector('.overlay-sizer').style['border-color'] = colours2017[newIndex]
        });
    };

    next2017 = next2017 || function () {
        offsetReplacement2017(offset => offset + 1, offset => offset >= 4)
    };

    prev2017 = prev2017 || function () {
        offsetReplacement2017(offset => offset - 1, offset => offset <= 0)
    };

    $(document).keydown((event) => {
        if (event.key === "ArrowLeft") {
            prev2017();
        } else if (event.key === "ArrowRight") {
            next2017();
        }
    });
}

document.querySelectorAll('#href-insertion-point.year2017 div.buttons .next').forEach(n => n.addEventListener('click', next2017));
document.querySelectorAll('#href-insertion-point.year2017 div.buttons .prev').forEach(n => n.addEventListener('click', prev2017));

$(document).ready(() => {
    $('.ui.dropdown')
        .dropdown();
})