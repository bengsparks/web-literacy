"use strict";

$(document).ready(() => {
    const items = $(".ui.secondary.menu > .item");

    items.on("click", function () {
        $(this)
            .addClass("active")
            .siblings()
            .removeClass("active")
        ;

        if ($(this).length !== 1) {
            throw `onClick .on registered ${$(this).length} elements instead of 1\n${$(this)}`;
        }

        const year = $(this)[0].innerHTML === "Impressum"
            ? "2021"
            : $(this)[0].innerHTML;

        console.log(year);

        progressBar.goToValue(year);
        loadDateAtInsertionPoint(year, "#href-insertion-point");
    });

    items[0].click();
});
